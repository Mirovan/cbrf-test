package ru.sbrftest.utils;

import ru.sbrftest.model.BNKseek;

import java.util.Date;

public class BNKSeekUtil {

    /**
     * Создает объект BNKSeek из массива объектов
     * */
    public static BNKseek generate(Object[] row) {
        String vkey = ((String) row[0]).trim();
        String real = ((String) row[1]).trim();
        String pzn = ((String) row[2]).trim();
        String uer = ((String) row[3]).trim();
        String rgn = ((String) row[4]).trim();
        String ind = ((String) row[5]).trim();
        String tnp = ((String) row[6]).trim();
        String nnp = ((String) row[7]).trim();
        String adr = ((String) row[8]).trim();
        String rkc = ((String) row[9]).trim();
        String namep = ((String) row[10]).trim();
        String namen = ((String) row[11]).trim();
        String newnum = ((String) row[12]).trim();
        String newks = ((String) row[13]).trim();
        String permfo = ((String) row[14]).trim();
        String spok = ((String) row[15]).trim();
        String at1 = ((String) row[16]).trim();
        String at2 = ((String) row[17]).trim();
        String telef = ((String) row[18]).trim();
        String regn = ((String) row[19]).trim();
        String okpo = ((String) row[20]).trim();
        Date dt_izm = (Date) row[21];
        String cks = ((String) row[22]).trim();
        String ksnp = ((String) row[23]).trim();
        Date date_in = (Date) row[24];
        Date date_ch = (Date) row[25];
        String vkeydel = ((String) row[26]).trim();
        Date dt_izmr = (Date) row[27];

        BNKseek record = new BNKseek();
        record.setVkey(vkey);
        record.setReal(real);
        record.setPzn(pzn);
        record.setUer(uer);
        record.setRgn(rgn);
        record.setInd(ind);
        record.setTnp(tnp);
        record.setNnp(nnp);
        record.setAdr(adr);
        record.setRkc(rkc);
        record.setNamep(namep);
        record.setNamen(namen);
        record.setNewnum(newnum);
        record.setNewks(newks);
        record.setPermfo(permfo);
        record.setSpok(spok);
        record.setAt1(at1);
        record.setAt1(at2);
        record.setTelef(telef);
        record.setRegn(regn);
        record.setOkpo(okpo);
        record.setDtIzm(dt_izm);
        record.setCks(cks);
        record.setKsnp(ksnp);
        record.setDateIn(date_in);
        record.setDateCh(date_ch);
        record.setVkeydel(vkeydel);
        record.setDtIzm(dt_izmr);

        return record;
    }

}
