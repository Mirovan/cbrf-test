package ru.sbrftest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.sbrftest.model.BNKseek;
import ru.sbrftest.service.BNKService;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    BNKService bnkService;


    /**
     * Главная страница
     * */
    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    /**
     * Отображение всех записей
     * */
    @RequestMapping(value="/show/all/", method = RequestMethod.GET)
    public ModelAndView showAll() {
        List<BNKseek> data = bnkService.read();
        String[] fields = bnkService.getFields();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fields", fields);
        modelAndView.addObject("data", data);
        modelAndView.setViewName("showdata");
        return modelAndView;
    }


    /**
     * Отображение 10 последних записей
     * */
    @RequestMapping(value="/show/last/", method = RequestMethod.GET)
    public ModelAndView showLast() {
        List<BNKseek> data = bnkService.read();
        List<BNKseek> lastData = data.subList(data.size()-10, data.size());
        String[] fields = bnkService.getFields();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fields", fields);
        modelAndView.addObject("data", lastData);
        modelAndView.setViewName("showdata");
        return modelAndView;
    }


    /**
     * Отображение с учетом фильтра
     * */
    @RequestMapping(value="/show/filter/", method = RequestMethod.GET)
    public ModelAndView showByFilter(@RequestParam(required = false) String search) {
        List<BNKseek> data = null;
        if (search != null)
            data = bnkService.readByFilter(search);
        else
            data = new ArrayList<>();

        String[] fields = bnkService.getFields();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fields", fields);
        modelAndView.addObject("data", data);
        modelAndView.setViewName("showdata");
        return modelAndView;
    }


    /**
     * Форма Добавление записи
     * */
    @RequestMapping(value="/add/", method = RequestMethod.GET)
    public ModelAndView add() {
        BNKseek record = new BNKseek();

        //sample data
        List<Integer> pznArr = new ArrayList<>();
        pznArr.add(30);
        pznArr.add(91);

        List<Integer> tnpArr = new ArrayList<>();
        tnpArr.add(1);
        tnpArr.add(2);

        List<Integer> uerArr = new ArrayList<>();
        uerArr.add(3);
        uerArr.add(4);

        List<Integer> regnArr = new ArrayList<>();
        regnArr.add(5);
        regnArr.add(6);

        //test
        record.setDateCh(Calendar.getInstance().getTime());
        record.setDateIn(Calendar.getInstance().getTime());
        record.setDtIzm(Calendar.getInstance().getTime());
        record.setDtIzmr(Calendar.getInstance().getTime());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("record", record);
        modelAndView.addObject("action", "add");
        modelAndView.addObject("pznArr", pznArr);
        modelAndView.addObject("tnpArr", tnpArr);
        modelAndView.addObject("uerArr", uerArr);
        modelAndView.addObject("regnArr", regnArr);
        modelAndView.setViewName("add");
        return modelAndView;
    }


    /**
     * Добавление записи
     * */
    @RequestMapping(value="/add/", method = RequestMethod.POST)
    public ModelAndView add(BNKseek record) {
        bnkService.add(record);

        return new ModelAndView("redirect:/show/last/");
    }


    /**
     * Удаление записи
     * */
    @RequestMapping(value="/delete/{newnum}/", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable String newnum) {
        bnkService.delete(newnum);

        return new ModelAndView("redirect:/show/filter/");
    }


    /**
     * Редактирование записи
     * */
    @RequestMapping(value="/edit/{newnum}/", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable String newnum) {
        bnkService.edit(newnum);

        return new ModelAndView("redirect:/show/filter/");
    }

}
