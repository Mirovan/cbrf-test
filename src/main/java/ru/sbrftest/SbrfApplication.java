package ru.sbrftest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbrfApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbrfApplication.class, args);
	}
}
