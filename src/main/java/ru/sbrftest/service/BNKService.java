package ru.sbrftest.service;

import ru.sbrftest.model.BNKseek;

import java.util.List;

public interface BNKService {
    List<BNKseek> read();
    List<BNKseek> readByFilter(String filter);
    void add(BNKseek record);
    String[] getFields();
    void delete(String newnum);
    void edit(String newnum);
}
