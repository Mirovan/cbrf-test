package ru.sbrftest.service;

import com.linuxense.javadbf.*;
import org.springframework.stereotype.Service;
import ru.sbrftest.model.BNKseek;
import ru.sbrftest.utils.BNKSeekUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static ru.sbrftest.utils.Constant.DATABASE;

@Service
public class BNKServiceImpl implements BNKService {

    @Override
    public List<BNKseek> read() {
        URL dbURL = this.getClass().getClassLoader().getResource(DATABASE);

        List<BNKseek> data = new ArrayList<>();

        DBFReader reader = null;
        try {
            String file = dbURL.toURI().getPath();
            reader = new DBFReader(new FileInputStream(file) );

            Object[] row;
            while ((row = reader.nextRecord()) != null) {
                BNKseek record = BNKSeekUtil.generate(row);

                data.add(record);
            }

        } catch (DBFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            DBFUtils.close(reader);
        }

        return data;
    }


    @Override
    public List<BNKseek> readByFilter(String filter) {
        URL dbURL = this.getClass().getClassLoader().getResource(DATABASE);

        List<BNKseek> data = new ArrayList<>();

        DBFReader reader = null;
        try {
            String file = dbURL.toURI().getPath();
            reader = new DBFReader(new FileInputStream(file) );

            int fieldCount = reader.getFieldCount();

            Object[] row;
            while ((row = reader.nextRecord()) != null) {
                boolean hasFilterField = false;
                for (int i=0; i<fieldCount; i++) {
                    if ( String.valueOf(row[i]).contains(filter) )
                        hasFilterField = true;
                }

                if (hasFilterField) {
                    BNKseek record = BNKSeekUtil.generate(row);
                    data.add(record);
                }
            }

        } catch (DBFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            DBFUtils.close(reader);
        }

        return data;
    }


    @Override
    public void add(BNKseek record) {
        URL dbURL = this.getClass().getClassLoader().getResource(DATABASE);
        try {
            DBFWriter writer = new DBFWriter(
                    new File(dbURL.toURI().getPath())
            );

            //set null field
            if (record.getVkey() == null) record.setVkey("");
            if (record.getReal() == null) record.setReal("");
            if (record.getPzn() == null) record.setPzn("");
            if (record.getUer() == null) record.setUer("");
            if (record.getRgn() == null) record.setRgn("");
            if (record.getInd() == null) record.setInd("");
            if (record.getTnp() == null) record.setTnp("");
            if (record.getNnp() == null) record.setNnp("");
            if (record.getAdr() == null) record.setAt1("");
            if (record.getRkc() == null) record.setRkc("");
            if (record.getNamep() == null) record.setNamep("");
            if (record.getNamen() == null) record.setNamen("");
            if (record.getNewnum() == null) record.setNewnum("");
            if (record.getNewks() == null) record.setNewks("");
            if (record.getPermfo() == null) record.setPermfo("");
            if (record.getSpok() == null) record.setSpok("");
            if (record.getAt1() == null) record.setAt1("");
            if (record.getAt2() == null) record.setAt2("");
            if (record.getTelef() == null) record.setTelef("");
            if (record.getRegn() == null) record.setRegn("");
            if (record.getOkpo() == null) record.setOkpo("");
            if (record.getDtIzm() == null) record.setDtIzm(Calendar.getInstance().getTime());
            if (record.getCks() == null) record.setCks("");
            if (record.getKsnp() == null) record.setKsnp("");
            if (record.getDateIn() == null) record.setDateIn(Calendar.getInstance().getTime());
            if (record.getDateCh() == null) record.setDateCh(Calendar.getInstance().getTime());
            if (record.getVkeydel() == null) record.setVkeydel("");
            if (record.getDtIzmr() == null) record.setDtIzmr(Calendar.getInstance().getTime());

            Object[] rowData = new Object[28];
            rowData[0] = record.getVkey();
            rowData[1] = record.getReal();
            rowData[2] = record.getPzn();
            rowData[3] = record.getUer();
            rowData[4] = record.getRgn();
            rowData[5] = record.getInd();
            rowData[6] = record.getTnp();
            rowData[7] = record.getNnp();
            rowData[8] = record.getAdr();
            rowData[9] = record.getRkc();
            rowData[10] = record.getNamep();
            rowData[11] = record.getNamen();
            rowData[12] = record.getNewnum();
            rowData[13] = record.getNewks();
            rowData[14] = record.getPermfo();
            rowData[15] = record.getSpok();
            rowData[16] = record.getAt1();
            rowData[17] = record.getAt2();
            rowData[18] = record.getTelef();
            rowData[19] = record.getRegn();
            rowData[20] = record.getOkpo();
            rowData[21] = record.getDtIzm();
            rowData[22] = record.getCks();
            rowData[23] = record.getKsnp();
            rowData[24] = record.getDateIn();
            rowData[25] = record.getDateCh();
            rowData[26] = record.getVkeydel();
            rowData[27] = record.getDtIzmr();

            writer.addRecord(rowData);

            writer.close();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String[] getFields() {
        URL dbURL = this.getClass().getClassLoader().getResource(DATABASE);

        String[] res = null;

        DBFReader reader = null;
        try {
            String file = dbURL.toURI().getPath();
            reader = new DBFReader(new FileInputStream(file) );

            int fieldCount = reader.getFieldCount();

            res = new String[fieldCount];

            for (int i = 0; i < fieldCount; i++) {
                DBFField field = reader.getField(i);
                res[i] = field.getName();
            }

        } catch (DBFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            DBFUtils.close(reader);
        }

        return res;
    }


    /**
     * Чтобы удалить какую либо запись, нужно прочитать все строки без удаляемой и заново сохранить в файл
     * */
    @Override
    public void delete(String newnum) {
        URL dbURL = this.getClass().getClassLoader().getResource(DATABASE);

        String[] res = null;

        DBFReader reader = null;
        try {
            String file = dbURL.toURI().getPath();
            reader = new DBFReader(new FileInputStream(file) );

            int fieldCount = reader.getFieldCount();

            res = new String[fieldCount];

            //Читаем все поля
            for (int i = 0; i < fieldCount; i++) {
                DBFField field = reader.getField(i);
                res[i] = field.getName();
            }

            //ToDo: читаем в список все строки, кроме newnum, сохраняем все файл (перезаписываем)

        } catch (DBFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            DBFUtils.close(reader);
        }
    }


    /**
     * Чтобы отредактировать какую либо запись, нужно прочитать все строки без редактирумой,
     * Редактируемую запись удаляем и заново сохранить в файл
     * */
    @Override
    public void edit(String newnum) {
        //ToDo: находим редактируемую запись и удаляем её, добавляем новую запись с учетом редактирования
    }
}
